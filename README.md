#FitnesseKit

包含两个项目:

## 1.Fitnesse

原始版本：20140630

代码来源：[https://github.com/unclebob/fitnesse/tree/20140630](https://github.com/unclebob/fitnesse/tree/20140630)

## 2.RestFixture

原始版本：3.1-SNAPSHOT

代码来源：[https://github.com/smartrics/RestFixture](https://github.com/smartrics/RestFixture)

## 3.下载、编译与运行步骤

请到[项目wiki页](http://git.oschina.net/fitneesefan/FitnesseKit/wikis/home)
