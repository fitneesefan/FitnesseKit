!define TEST_SYSTEM {slim}

!path D:\git\FitnesseKit\RestFixture\target\dependencies\*
!path D:\git\FitnesseKit\RestFixture\target\classes
!path D:\git\FitnesseKit\RestFixture\extra\slf4j-simple-1.6.6.jar

| Table:smartrics.rest.fitnesse.fixture.RestFixture | http://www.w3school.com.cn |
| GET | /example/xmle/note.xml | 200 | Content-Type: text/xml | //body[text()="Don't forget the meeting!"]|

| Table:smartrics.rest.fitnesse.fixture.RestFixture | http://www.w3school.com.cn |
| GET | /example/xmle/note.xml | 200 | Content-Type: text/xml | /note/body[text()="Don't forget the meeting!"]|


